# QFN48-nRF52811-Bluetooth-5.1-KiCad and Altium

The original schematic and files were designed and downloaded as Altium designer files. Afte they were edited to make the dev board, this repo was used: https://github.com/thesourcerer8/altium2kicad to convert the altium designer files from altium to KiCad. Then, these files still neded editing in KiCad, eventyally leading to the filal KiCad file. It is reccomended to use the Altium files if you intend to print of use the board, but if you insist on KiCad, just check for errors (unconnected pads) on the pcb, if you spot any fix ut, email me for the fix or open an issue and you will be good to go. I would be more than happy if anyone checked the KiCad files and aproved their functionality, or design the board from the beggining for KiCad. Its not very complex circuit, last time i checked the KiCad pcb was file, wihtout weird connections

Dc-Dc schematic From datasheet: [![Untitled.png](https://i.postimg.cc/6qh7fCR8/Untitled.png)](https://postimg.cc/PvPrtpSH)

Edited Altium Designer PCB [dc-dc schematic] as a dev board, 3D view: [![Altium3d.png](https://i.postimg.cc/XYm8kRCC/Altium3d.png)](https://postimg.cc/G4PGcSzL)

Edited Altium Designer PCB [dc-dc schematic] as a dev board, 2D view: [![Altium.png](https://i.postimg.cc/5N9n4qJ6/Altium.png)](https://postimg.cc/bdMQLnsh)

Edited KiCad PCB, 2D [![antenna.jpg](https://i.postimg.cc/zB9XjBJt/antenna.jpg)](https://postimg.cc/MMmSZzfV)

Edited KiCad PCB, 3D [![antennagnd.jpg](https://i.postimg.cc/SQMKKWrw/antennagnd.jpg)](https://postimg.cc/xq9QxNBR)

REFERENCE LAYOUT FOUND HERE [QFN48]: https://www.nordicsemi.com/?sc_itemid=%7B7CBDF55C-9745-4A7D-A491-5B66AE9454B6%7D
